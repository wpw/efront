# 全局设置
```sh
$ git config --global user.email "wpw@travelsky.com"
$ git config --global user.name wpw
$ git config --global credential.helper store
$ git config --global core.autocrlf false
```

# 克隆远程仓库
```sh
$ git clone git@gitlab.com:wupanwei/efront.git 
正克隆到 'efront'...
warning: 您似乎克隆了一个空仓库。
检查连接... 完成。
```

# 查看状态
```sh
$ cd efront
$ git status
位于分支 master

初始提交

无文件要提交（创建/拷贝文件并使用 "git add" 建立跟踪）
```

# 创建文件夹及文件
```
$ mkdir git
$ cd git
$ vi git.basic.md
```


# 再次查看状态
```sh
$ cd ../
$ ll
总用量 4.0K
drwxrwxr-x 2 wpw wpw 4.0K Mar 29 15:48 git
$ git status
位于分支 master

初始提交

未跟踪的文件:
  （使用 "git add <文件>..." 以包含要提交的内容）

        git/

提交为空，但是存在尚未跟踪的文件（使用 "git add" 建立跟踪）
```

# 将文件添加到git暂存区，以便git跟踪
```sh
$ git add git
$ git status
位于分支 master

初始提交

要提交的变更：
  （使用 "git rm --cached <文件>..." 以取消暂存）

        新文件：   git/git.basic.md

```

# 提交文件到本地仓库
```sh
$ git commit -m "add files"
[master （根提交） 66660ae] add files
 1 file changed, 9 insertions(+)
 create mode 100644 git/git.basic.md
$ git status
位于分支 master
您的分支基于 'origin/master'，但此上游分支已经不存在。
  （使用 "git branch --unset-upstream" 来修复）
无文件要提交，干净的工作区
```

# 推送文件到远程仓库
```sh
$ git push --set-upstream origin master
对象计数中: 4, 完成.
Delta compression using up to 4 threads.
压缩对象中: 100% (2/2), 完成.
写入对象中: 100% (4/4), 430 bytes | 0 bytes/s, 完成.
Total 4 (delta 0), reused 0 (delta 0)
To git@gitlab.com:wupanwei/efront.git
 * [new branch]      master -> master
分支 master 设置为跟踪来自 origin 的远程分支 master。
```

# 查看历史
```sh
$ git log
commit 66660aec35980e21ddbab2c9529fa27c5c21fe3f
Author: wupanwei <wpw@travelsky.com>
Date:   Thu Mar 29 15:50:54 2018 +0800

    add files
```

# 在gitlab上增加一个文件
> 使用页面编辑器在efront库的git目录下增加一个 README.md,里面的内容是： git pull

# 从远程仓库抓取数据
```sh
$  git pull
remote: Counting objects: 4, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 4 (delta 0), reused 0 (delta 0)
展开对象中: 100% (4/4), 完成.
来自 gitlab.com:wupanwei/efront
   66660ae..79b2fd1  master     -> origin/master
更新 66660ae..79b2fd1
Fast-forward
 git/README.md | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 git/README.md
```

# 在本地git目录下修改一个文件
> 在本地git目录下修改 README.md,增加的内容是： git diff HEAD
```sh
$ git diff HEAD
diff --git a/git/README.md b/git/README.md
index bddc158..a144916 100644
--- a/git/README.md
+++ b/git/README.md
@@ -1 +1,2 @@
-git pull
\ No newline at end of file
+git pull
+git diff HEAD
\ No newline at end of file
```

# 将修改的文件添加到暂存区，以便跟踪
```sh
$ git status
位于分支 master
您的分支与上游分支 'origin/master' 一致。
尚未暂存以备提交的变更：
  （使用 "git add <文件>..." 更新要提交的内容）
  （使用 "git checkout -- <文件>..." 丢弃工作区的改动）

        修改：     git/README.md

修改尚未加入提交（使用 "git add" 和/或 "git commit -a"）
$ git add -A
```

# 查看暂存区与本地库之间的差异
```sh
$ git diff --staged
diff --git a/git/README.md b/git/README.md
index bddc158..a144916 100644
--- a/git/README.md
+++ b/git/README.md
@@ -1 +1,2 @@
-git pull
\ No newline at end of file
+git pull
+git diff HEAD
\ No newline at end of file
```

# 重置暂存区中的文件
```sh
$ git reset git/README.md
重置后取消暂存的变更：
M       git/README.md
```

# 将工作区的文件还原到修改之前
```sh
$ git status
位于分支 master
您的分支与上游分支 'origin/master' 一致。
尚未暂存以备提交的变更：
  （使用 "git add <文件>..." 更新要提交的内容）
  （使用 "git checkout -- <文件>..." 丢弃工作区的改动）

        修改：     git/README.md

修改尚未加入提交（使用 "git add" 和/或 "git commit -a"）
$ git checkout -- git/README.md
$ git status
位于分支 master
您的分支与上游分支 'origin/master' 一致。
无文件要提交，干净的工作区
```

# 创建并切换分支
```sh
$ git branch dev
$ git checkout dev
M       git/README.md
切换到分支 'dev'
```

# 推送新的分支到远程仓库
```sh
$ git push --set-upstream origin dev
Total 0 (delta 0), reused 0 (delta 0)
remote:
remote: To create a merge request for dev, visit:
remote:   https://gitlab.com/wupanwei/efront/merge_requests/new?merge_request%5Bsource_branch%5D=dev
remote:
To git@gitlab.com:wupanwei/efront.git
 * [new branch]      dev -> dev
分支 dev 设置为跟踪来自 origin 的远程分支 dev。
```

# 删除当前分支上的文件
```sh
$ git rm '*.md'
rm 'git/README.md'
rm 'git/git.basic.md'
```

# 提交修改到当前分支
```sh
$ git commit -m "Remove all the cats"
[dev 66e2abb] Remove all the cats
 2 files changed, 10 deletions(-)
 delete mode 100644 git/README.md
 delete mode 100644 git/git.basic.md
```

# 切换到master分支
```sh
$ git checkout master
切换到分支 'master'
您的分支与上游分支 'origin/master' 一致。
```

# 合并dev分支
```sh
$ git merge dev
更新 79b2fd1..66e2abb
Fast-forward
 git/README.md    | 1 -
 git/git.basic.md | 9 ---------
 2 files changed, 10 deletions(-)
 delete mode 100644 git/README.md
 delete mode 100644 git/git.basic.md
$  ls
```

# 删除分支
```sh
$ git branch -d dev
warning: 并未删除分支 'dev'， 虽然它已经合并到 HEAD，
         然而却尚未被合并到分支 'refs/remotes/origin/dev' 。
error: 分支 'dev' 没有完全合并。
如果您确认要删除它，执行 'git branch -D dev'。
$  git branch -D dev
已删除分支 dev（曾为 66e2abb）。
```


# 推送文件到远程仓库
```sh
$ git push
对象计数中: 2, 完成.
写入对象中: 100% (2/2), 196 bytes | 0 bytes/s, 完成.
Total 2 (delta 0), reused 0 (delta 0)
To git@gitlab.com:wupanwei/efront.git
   79b2fd1..66e2abb  master -> master
```


# 回退提交
```sh
$ git revert HEAD
[master 9f4bb31] Revert "Remove all the cats"
 2 files changed, 10 insertions(+)
 create mode 100644 git/README.md
 create mode 100644 git/git.basic.md
$ ll
总用量 4.0K
drwxrwxr-x 2 wpw wpw 4.0K Mar 29 16:40 git
```

# 再次推送到远程仓库
```sh
$ git push
Delta compression using up to 4 threads.
压缩对象中: 100% (3/3), 完成.
写入对象中: 100% (5/5), 538 bytes | 0 bytes/s, 完成.
Total 5 (delta 0), reused 0 (delta 0)
To git@gitlab.com:wupanwei/efront.git
   66e2abb..9f4bb31  master -> master
```


# 解决冲突
> 准备新的feature1分支，继续我们的新分支开发：
```sh
$ git checkout -b feature1
切换到一个新分支 'feature1'
```

> 修改readme.txt最后一行，改为：
```txt
  Creating a new branch is quick AND simple.
```

> 在feature1分支上提交：
```sh
➜  efront git:(feature1) ✗ git add readme.txt
➜  efront git:(feature1) ✗ git commit -m "AND simple"
[feature1 d1326fe] AND simple
 1 file changed, 1 insertion(+)
 create mode 100644 readme.txt
```

> 切换到master分支：
```sh
$ git checkout master
切换到分支 'master'
您的分支与上游分支 'origin/master' 一致。
```

> 修改readme.txt最后一行，改为：
```txt
Creating a new branch is quick & simple.
```

> 提交
```sh
$ git commit -m "& simple"
[master 6a1e920] & simple
 1 file changed, 1 insertion(+)
 create mode 100644 readme.txt
```

> 现在，master分支和feature1分支各自都分别有新的提交,这种情况下，Git无法执行“快速合并”，只能试图把各自的修改合并起来，但这种合并就可能会有冲突，我们试试看：
```sh
$ git merge feature1
自动合并 readme.txt
冲突（添加/添加）：合并冲突于 readme.txt
自动合并失败，修正冲突然后提交修正的结果。
```

> 果然冲突了！Git告诉我们，readme.txt文件存在冲突，必须手动解决冲突后再提交。git status也可以告诉我们冲突的文件：
```sh
$ git status
位于分支 master
您的分支领先 'origin/master' 共 1 个提交。
  （使用 "git push" 来发布您的本地提交）
您有尚未合并的路径。
  （解决冲突并运行 "git commit"）

未合并的路径：
  （使用 "git add <文件>..." 标记解决方案）

        双方添加：   readme.txt

修改尚未加入提交（使用 "git add" 和/或 "git commit -a"）
```

> 我们可以直接查看readme.txt的内容：
```sh
$ cat readme.txt
<<<<<<< HEAD
Creating a new branch is quick & simple.
=======
Creating a new branch is quick AND simple.
>>>>>>> feature1
```

> Git用<<<<<<<，=======，>>>>>>>标记出不同分支的内容，我们修改如下后保存：
```txt
Creating a new branch is quick and simple.
```

> 再提交
```
$ git add readme.txt
$ git commit -m "conflict fixed"
[master 3b24435] conflict fixed
```

> 用带参数的git log也可以看到分支的合并情况：
```txt
*   3b24435 conflict fixed
|\
| * d1326fe AND simple
* | 6a1e920 & simple
|/
* 9f4bb31 Revert "Remove all the cats"
* 66e2abb Remove all the cats
* 79b2fd1 Add new file
* 66660ae add files
```

# 最后，删除feature1分支：
```sh
$ git branch -d feature1
已删除分支 feature1（曾为 d1326fe）。
```
